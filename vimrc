set nocompatible
syntax on

filetype off
set smartindent
set shiftwidth=4

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" ----- Making Vim look good ------------------------------------------
Plugin 'altercation/vim-colors-solarized'
Plugin 'tomasr/molokai'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'xuyuanp/nerdtree-git-plugin'

Plugin 'vim-syntastic/syntastic'


Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-easytags'
Plugin 'majutsushi/tagbar'

Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-scripts/a.vim'

Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'shumphrey/fugitive-gitlab.vim'

Plugin 'Raimondi/delimitMate'
Plugin 'jez/vim-superman'

Plugin 'jlanzarotta/bufexplorer'
Plugin 'exvim/ex-utility'
" Plugin 'exUtility'

"Plugin 'jez/vim-c0'
"Plugin 'jez/vim-ispc'
"
"RUST
"Plugin 'neoclide/coc.nvim', {'branch': 'release'}
"Plugin 'tpope/vim-rhubarb'
"Plugin 'neoclide/coc-rls'
"Plugin 'rust-lang/rust.vim'
"Plugin 'timonv/vim-cargo'


Plugin 'flazz/vim-colorschemes'


Plugin 'christoomey/vim-tmux-navigator'

call vundle#end()

filetype plugin indent on

" General settings
set backspace=indent,eol,start
set ruler
set number
set showcmd
set incsearch
set hlsearch

syntax on

" Enable mouse
set mouse=a

" We need this for plugins like Syntastic and vim-gitgutter which put symbols
" " in the sign column.
" hi clear SignColumn
"

" ----- Plugin-Specific Settings --------------------------------------
" ----- altercation/vim-colors-solarized settings -----
"
colorscheme onehalfdark
" lightline
" let g:lightline = { 'colorscheme': 'onehalfdark' }

" Toggle this to "light" for light colorscheme
set background=dark

" ----- bling/vim-airline settings -----
" Always show statusbar
set laststatus=2

" Fancy arrow symbols, requires a patched font
" To install a patched font, run over to
"     https://github.com/abertsch/Menlo-for-Powerline
" download all the .ttf files, double-click on them and click "Install"
" Finally, uncomment the next line
let g:airline_powerline_fonts = 1

" Show PASTE if in paste mode
let g:airline_detect_paste=1

" Show airline for tabs too
let g:airline#extensions#tabline#enabled = 1

" Use the solarized theme for the Airline status bar
" let g:airline_theme='jellybeans'
let g:airline_theme='onehalfdark'

" ----- jistr/vim-nerdtree-tabs -----
" Open/close NERDTree Tabs with \t
nmap <silent> <leader>t :NERDTreeTabsToggle<CR>
" To have NERDTree always open on startup
let g:nerdtree_tabs_open_on_console_startup = 1
let g:NERDTreeWinPos = "right"


" ----- scrooloose/syntastic settings -----
let g:syntastic_error_symbol = '✘'
let g:syntastic_warning_symbol = "▲"
augroup mySyntastic
	au!
au FileType tex let b:syntastic_mode = "passive"
augroup END

" ----- xolox/vim-easytags settings -----
" Where to look for tags files
set tags=./tags;,~/.vimtags
" Sensible defaults
let g:easytags_events = ['BufReadPost', 'BufWritePost']
let g:easytags_async = 1
let g:easytags_dynamic_files = 2
let g:easytags_resolve_links = 1
let g:easytags_suppress_ctags_warning = 1

" ----- majutsushi/tagbar settings -----
" Open/close tagbar with \b
nmap <silent> <leader>b :TagbarToggle<CR>
" Uncomment to open tagbar automatically whenever possible
autocmd BufEnter * nested :call tagbar#autoopen(0)

" Ctrlp -> enter or ctrl t
" at: test.h <-> test.c

" ----- airblade/vim-gitgutter settings -----
" In vim-airline, only display "hunks" if the diff is non-zero
let g:airline#extensions#hunks#non_zero_only = 1

" git add                  --> :Gwrite
" git commit               --> :Gcommit
" git push                 --> :Gpush
" git checkout <file name> --> :Gread
" git blame                --> :Gblame
" git status		   --> :Gstatus
"
"
" ----- Raimondi/delimitMate settings -----
let delimitMate_expand_cr = 1
augroup mydelimitMate
au!
	au FileType markdown let b:delimitMate_nesting_quotes = ["`"]
	au FileType tex let b:delimitMate_quotes = ""
	au FileType tex let b:delimitMate_matchpairs = "(:),[:],{:},`:'"
	au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]
augroup END

" ----- jez/vim-superman settings -----
" better man page support
noremap K :SuperMan <cword><CR>

"
"'be' (normal open) or 'bt' (toggle open / close) or 'bs' (force horizontal
" split open) or 'bv' (force vertical split open)
"
" Rust tags:
autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/
autocmd BufWritePost *.rs :silent! exec "!rusty-tags vi --quiet --start-dir=" . expand('%:p:h') . "&" | redraw!
